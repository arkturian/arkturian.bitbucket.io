
// Игра "Переправа через реку", автор Игорь.
// доступна онлайн по адресу: http://arkturian.bitbucket.org
// исходный код здесь: http://bitbucket.org/arkturian/arkturian.bitbucket.org
// код предоставлен под лицензией MIT.


// Прототип для всех пассажиров. После унаследования этого
// прототипа, пассажир тоже будет иметь все эти свойства.
// Наследование делаю для того, чтобы не повторять весь этот код
// 4 раза для каждого пассажира.
var prototip = {
    // левый берег как начальное положение для всех пассажиров.
    gde: 'levo',
    // координаты смещения для всех пассажиров на левом и правом
    // берегах по осям X и Y. [0, 0] означает начальное положение.
    bereg_levo: [0, 0],
    bereg_pravo: [730, 0],
    mesto_v_lodke: 0,
}

// Пассажиры
var ded = {
    kto: 'ded',
    // координаты X и Y для смещения картинки на первое и
    // второе место лодки для левого и правого берегов.
    levo1: [245, 40],
    levo2: [315, 40],
    pravo1: [535, 40],
    pravo2: [605, 40],
}
var volk = {
    kto: 'volk',
    levo1: [130, 40],
    levo2: [200, 40],
    pravo1: [420, 40],
    pravo2: [490, 40],
}
var kozel = {
    kto: 'kozel',
    // Отрицительные координаты оси Y означают
    // поднятие картинки вверх, а не спуск вниз.
    levo1: [245, -110],
    levo2: [315, -110],
    pravo1: [535, -110],
    pravo2: [605, -110],
}
var kapusta = {
    kto: 'kapusta',
    levo1: [130, -110],
    levo2: [200, -110],
    pravo1: [420, -110],
    pravo2: [490, -110],
}
var lodka = {
    gde: 'levo',
    levo: 0,
    pravo: 290,
    passazhiri: [],    
}
var igroki = [volk, kozel, kapusta, ded];

lodka.kakoe_mesto_svobodno = function () {
    if ( lodka.passazhiri.length > 1 ) {
        // Свободных мест нет.
        alert("Лодка не резиновая, в ней всего 2 места.\n Хотя... может быть, она и резиновая");
        console.log("v lodke 2 mesta, oba zanyati.")
        return false;
    }
    if ( lodka.passazhiri.length < 1 ) {
        // Два места свободны - возвращаю индекс на первое.
        return 1;
    }
    // Если два предыдущих return-a не сработали, значит
    // в лодке есть только одно свободное место.
    // Нужно определить, какое именно место в лодке свободно,
    // для этого нахожу пассажира уже сидящего в лодке
    // и считываю его mesto_v_lodke.
    var passazhir_v_lodke = eval(lodka.passazhiri[0]);
    var zanyatoe_mesto = passazhir_v_lodke.mesto_v_lodke;
    // Если занято первое место, возвращаю двойку.
    // иначе, возвращаю единицу.
    return ( zanyatoe_mesto === 1 ) ? 2 : 1;
}

lodka.vzyat_na_bort = function (passazhir) {
    // добавляю имя загружаемого пассажира.
    lodka.passazhiri.push(passazhir.kto);
}
lodka.za_bort = function (passazhir) {
    // освобождаю в лодке место занятое пассажиром.
    // для этого, нахожу номер индекса пассажира и
    // удаляю по индексу одного пассажира из лодки.
    var index = lodka.passazhiri.indexOf(passazhir.kto);
    lodka.passazhiri.splice(index, 1);
}

var peredvinut_kartinku = function (name, koordinati, razmer) {
    move('#' + name)
    .translate(
    koordinati[0],
    koordinati[1])
    .scale(razmer)
    .end();
}

// Загрузка пассажира в лодку.
var zagruzit = function () {
    // Служебная функция eval находит в web-странице
    // переменную по строке c её названием.    
    // Про слово "this": эту функцию zagruzit я привяжу
    // к клику по картинке пассажира, поэтому слово this
    // будет указывать на ту картинку. 
    // Слово this даст мне картинку, по которой кликнули
    // мышкой, из этого html-элемента картинки я достаю свойство
    // id (например строка "volk"), эту строку толкаю в
    // служебную функцию eval, которая найдёт переменную (объёкт) в
    // моём javascript'e с этим именем (volk).
    var passazhir = eval(this.id);
    var svobodnoe_mesto = lodka.kakoe_mesto_svobodno();
    if (svobodnoe_mesto && lodka.gde === passazhir.gde) {
        // console.log(svobodnoe_mesto);
        var koordinati = passazhir[passazhir.gde + svobodnoe_mesto];
        peredvinut_kartinku(passazhir.kto, koordinati, 0.6);

        lodka.vzyat_na_bort(passazhir);

        // делаю уведомление в лог. увидеть логи можно в браузере, 
        // нажав F12, вкладка Консоль.
        console.log('zagruzil. passazhiri: ', lodka.passazhiri);
        // указываю какой номер места (1 или 2) занял пассажир.
        passazhir.mesto_v_lodke = svobodnoe_mesto;
        // меняю onclick пассажира на выгрузку обратно на берег.
        $('#' + passazhir.kto).onclick = vigruzit;
    }
}
// Выгрузка пассажира из лодки.
var vigruzit = function () {
    var passazhir = eval(this.id);

    var koordinati = passazhir['bereg_' + passazhir.gde];
    peredvinut_kartinku(passazhir.kto, koordinati, 1);

    lodka.za_bort(passazhir);

    console.log('vigruzil. passazhiri: ', lodka.passazhiri);
    passazhir.mesto_v_lodke = 0;

    // пассажир выгружен. меняю привязку клика по пассажиру на загрузку
    $('#' + passazhir.kto).onclick = zagruzit;

    // при выгрузке из лодки Деда, проверяю: все ли на правом берегу?
    // если да, то игра успешно завершена.
    if ( passazhir.kto === "ded" ) {
        proverka_igra_okon4ena();
    }
}

lodka.dvigatsya = function (napravlenie) {
    move('#lodka').x(lodka[napravlenie]).end();
    lodka.gde = napravlenie;
}
lodka.dvigat_vseh_passazhirov = function (napravlenie) {
    for ( var index in lodka.passazhiri ) {
        dvigat_passazhira(lodka.passazhiri[index], napravlenie);
    }
}

var dvigat_passazhira = function (name, napravlenie) {
    var passazhir = eval(name);
    
    console.log("dvigayu passazhira ", napravlenie, passazhir.kto); 

    var koordinati = passazhir[napravlenie + passazhir.mesto_v_lodke];
    peredvinut_kartinku(passazhir.kto, koordinati, 0.6);

    // Пассажир перевезён, меняю его берег.
    passazhir.gde = napravlenie;
    // console.log(marshrut, passazhir);
}

var perekluchit_knopki = function (marshrut, napravlenie) {
    // использую функцию селектора $() для нахождения html-элементов
    // кнопок на веб-странице и меняю их CSS-свойства.
    $('.knopka#'+marshrut[napravlenie]).style.visibility = "hidden";
    $('.knopka#'+napravlenie).style.visibility = "visible";
}

var poehali = function () {
    var marshrut = {levo:'pravo', pravo:'levo'};
    var napravlenie = marshrut[lodka.gde];
    // console.log("v poehali() ", napravlenie, lodka[napravlenie]);
    if ( vse_proverki() ) {
        lodka.dvigatsya(napravlenie);
        lodka.dvigat_vseh_passazhirov(napravlenie);
        // Прячу нажатую на берегу кнопку, показываю другую.
        perekluchit_knopki(marshrut, napravlenie);
    }
}

// Проверки перед отправкой лодки.
var ne_v_lodke = function (igrok) {
    return ( igrok.mesto_v_lodke === 0 ) ? true : false;
}

var na_odnom_beregu = function (igrok1, igrok2) {
    // igroki.every()
    if ( ne_v_lodke(igrok1) && ne_v_lodke(igrok2) ) {
        if ( igrok1.gde === igrok2.gde ) {
            return true;
        }
    }
    return false;
}

var proverka_est_li_v_lodke_ded = function () {
    if ( ne_v_lodke(ded) ) {
        alert("Только Дед умеет управлять лодкой");
        // console.log("ded dolzhen bit v lodke");
        return false;
    }
    return true;
}

var proverka_kozel_kapusta = function () {
    if ( na_odnom_beregu(kozel, kapusta) )  {
        alert('Без Деда, Козёл потратит всю капусту на козочек');
        return false;
    }
    return true;
}

var proverka_volk_kozel = function () {
    if ( na_odnom_beregu(volk, kozel) ) {
        alert('Без Деда, Волк с Козлом подерутся');
        return false;
    }
    return true;    
}

var vse_proverki = function () {
    if ( proverka_est_li_v_lodke_ded() 
         && proverka_kozel_kapusta() 
         && proverka_volk_kozel() ) {
            return true;
    }
    return false;
}

var proverka_igra_okon4ena = function() {
    var kto_na_pravom_beregu = igroki.filter( function (igrok) {
        return ( igrok.gde === "pravo" ) ? true : false;
    });

    // Если все четверо на правом берегу, то игра успешно завершена.
    if ( kto_na_pravom_beregu.length === 4 ) {
       alert("  Поздравляю!\nВы справились!"); 
    }
}

// Главная функция - запускает игру.
// Выполняет наследование прототипа для каждого
// пассажира и привязывает функции к нажатиям мыши 
// по картинкам и кнопкам на html-странице.
var glavnaya = function () {
    for ( var index in igroki) {
        var igrok = igroki[index];
        // наследование прототипа для текущего игрока.
        igrok.__proto__ = prototip;

        $('#' + igrok.kto).onclick = zagruzit;
    }
    // привязываю функцию poehali() к нажатию по кнопкам.
    $('.knopka#levo').onclick = poehali;
    $('.knopka#pravo').onclick = poehali;
}

// Вспомогательная функция для удобного поиска элементов
// html-страницы по их HTML или CSS идентификаторам.
// Увидел её в документации к библиотеке move.js,
// которую я использую для передвижения картинок.
// Работает так: $('#id_html_элемента'), например:
// $('#ded') или $('.knopka#levo').
// Адрес сайта библиотеки move.js:
// https://visionmedia.github.io/move.js/
function $ (selector) {
    var elements = document.querySelectorAll(selector);
    if (elements.length > 0) {
        return (elements.length === 1) ? elements[0] : elements;
    }    
    console.log('ERROR: selector ' + selector + ' not found.');
    return null;
}